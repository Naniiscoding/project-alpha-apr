from django.shortcuts import render, redirect
from projects.models import Project
from projects.forms import ProjectForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def list_projects(request):
    lists = Project.objects.filter(owner=request.user)
    context = {"lists": lists}
    return render(request, "projects/list.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            Project = form.save(False)
            Project.members = request.user
            Project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)


@login_required
def show_project(request, id):
    detail = Project.objects.get(id=id)
    context = {"detail": detail}
    return render(request, "projects/detail.html", context)
