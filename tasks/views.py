from django.shortcuts import render
from tasks.models import Task

# Create your views here.


def create_Task(request, id):
    list = Task.objects.get(id=id)
    context = {
        "list": list,
    }
    return render(request, "tasks/my_tasks.html", context)
